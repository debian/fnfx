/*
 * Author: Timo Hoenig <thoenig@nouse.net>
 * Copyright (c) 2003, 2004 Timo Hoenig <thoenig@nouse.net>
 *                          All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

/*
 * Defines
 *
 */

/*
 * Declarations
 *
 */

void walk_config(void);
int do_action(int);

/*
 * Includes
 *
 */

#include "fnfx.h"

/*
 * External functions 
 *
 */

extern void debug(const char *fmt, ...) __attribute__ ((format(printf, 1, 2)));
extern void fatal(const char *fmt, ...) __attribute__ ((format(printf, 1, 2)));
extern int brightness_up(void);
extern int brightness_down(void);
extern int fan_toggle(void);
extern int video_toggle(int);
extern int volume_up(void);
extern int volume_down(void);
extern int mute(void);
extern int suspend(int);
extern int cpu_toggle(void);
extern int bluetooth_toggle(void);

/*
 * External variables
 *
 */

extern t_daemon_cfg *cfg;

/*
 * Globals
 *
 */

/*
 * Functions
 *
 */

/*
 * do_action()
 *
 * Execute an internal action
 *
 */

int do_action(int i)
{
    char *command;

    if (cfg->keymap.fnkey[i].state & FNKEY_CLIENT)
        command = cfg->keymap.fnkey[i].client_command;
    else
        command = cfg->keymap.fnkey[i].command;

    if (!(strcmp("brightness up", command))) {
        if (brightness_up())
            return 1;
        else
            return 0;
    }
    else if (!(strcmp("brightness down", command))) {
        if (brightness_down())
            return 1;
        else
            return 0;
    }
    else if (!(strcmp("toggle fan", command))) {
        if (fan_toggle())
            return 1;
        else
            return 0;
    }
    else if (!(strcmp("toggle video", command))) {
        if (video_toggle(VIDEO_LCD))
            return 1;
        else
            return 0;
    }
    else if (!(strcmp("toggle tv", command))) {
        if (video_toggle(VIDEO_TV))
            return 1;
        else
            return 0;
    }
    else if (!(strcmp("volume up", command))) {
        if (volume_up())
            return 1;
        else
            return 0;
    }
    else if (!(strcmp("volume down", command))) {
        if (volume_down())
            return 1;
        else
            return 0;
    }
    else if (!(strcmp("mute", command))) {
        if (mute())
            return 1;
        else
            return 0;
    }
    else if (!(strcmp("suspend to ram", command))) {
        if (suspend(3))
            return 1;
        else
            return 0;
    }
    else if (!(strcmp("suspend to disk", command))) {
        if (suspend(4))
            return 1;
        else
            return 0;
    }
    else if (!(strcmp("toggle cpu", command))) {
        if (cpu_toggle())
            return 1;
        else
            return 0;
    }
    else if (!(strcmp("toggle bluetooth", command))) {
        if (bluetooth_toggle())
            return 1;
        else
            return 0;
    }

    return 0;
}

/*
 * config_walk()
 *
 * Just 'walk' the config struct and print as debug messages
 *
 */

void walk_config()
{
    struct shmid_ds shm_info;
    int i = 0;

    debug("walk_config() - Starting config walk...");
    debug(" ");

    debug("I General Information");

    debug("-> FnFX Daemon PID is %i (getpid() = %i).", cfg->pid, getpid());
    debug("-> Command line arguments: Debug is %s, Nodetach is %s.",
          (cfg->debug_flag) ? "ON" : "OFF",
          (cfg->nodetach_flag) ? "ON" : "OFF");
    debug("-> Configuration size is %i bytes.", sizeof(*cfg));
    debug(" ");

    debug("II Shared Memory");

    if (shmctl(cfg->shmem.id, IPC_STAT, &shm_info) == -1) {
        debug
            ("Error while retrieving shared memory information. Aborting walk_config().");
        return;
    }
    else

        debug
            ("-> Shared memory segment is located at 0x%x (size: %i bytes, id: %i, key: 0x%x).",
             (unsigned int) cfg->shmem.addr, shm_info.shm_segsz, cfg->shmem.id,
             cfg->shmem.key);
    debug("-> Daemon configuration is located at 0x%x.", (unsigned int) cfg);
    debug(" ");

    debug("III ACPI");
    debug("-> The following ACPI proc keys are available:");
    for (i = 0; i < 6; i++)
        if ((cfg->acpi.config >> i) & 1)
            switch (i) {
            case 0:
                debug("   %s", ACPI_TOSHIBA);
                break;
            case 1:
                debug("   %s", ACPI_FAN);
                break;
            case 2:
                debug("   %s", ACPI_KEYS);
                break;
            case 3:
                debug("   %s", ACPI_LCD);
                break;
            case 4:
                debug("   %s", ACPI_VERSION);
                break;
            case 5:
                debug("   %s", ACPI_VIDEO);
                break;
            default:
                break;
            }
    debug(" ");

/*	debug("KEYMAP");
	for(i = 0; i < cfg->keymap.entries; i++)
		debug("-> Key value = 0x%x, Description = %s. Command: %s.", \
				cfg->keymap.fnkey[i].value, cfg->keymap.fnkey[i].descr, cfg->keymap.fnkey[i].command);

	debug("-> There are %i valid keys registered.", cfg->keymap.entries);
	debug(" ");
*/

    debug("IV ACTIONS");
    for (i = 0; i < cfg->keymap.entries; i++)

        if (cfg->keymap.fnkey[i].state & FNKEY_ENABLED)
            debug("-> %s (value = 0x%x) will execute \"%s\".",
                  cfg->keymap.fnkey[i].descr,
                  cfg->keymap.fnkey[i].value, cfg->keymap.fnkey[i].command);
    debug(" ");

    debug("VI Semaphore");
    debug("-> Semaphore id is %i.", cfg->sem.id);
    debug("-> Semaphore value is %i (<- Should really be 0.).",
          semctl(cfg->sem.id, 0, GETVAL));
    debug(" ");
}
