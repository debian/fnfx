/* fnfxd.c - FnFX daemon
 * 
 * Author: Timo Hoenig <thoenig@nouse.net>
 * Copyright (c) 2003, 2004 Timo Hoenig <thoenig@nouse.net>
 *                          All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

/*
 * Defines
 *
 */

#define PROGRAM		"FnFX Daemon"
#define	VERSION		"v0.3"
#define AUTHOR		"Timo Hoenig <thoenig@nouse.net>"

/*
 * Includes
 *
 */

#include "fnfx.h"

/*
 * Declarations
 *
 */

void debug(const char *, ...);
void fatal(const char *, ...);

void version(void);
void information(void);
void usage(void);
int check_uid(void);
int init(int *, int *);
void loop(void);

/*
 * External functions 
 *
 */

/* fnfxd_init.c */
extern int init_sig_handler(void);
extern int init_fnfxd(int *, int *);
extern int chk_fnkey(void);
extern void walk_config(void);

/* fnfxd_cmds.c */
extern int brightness_status(void);
extern int fan_status(void);
extern int video_status(void);
extern int set_brightness(int);

/*
 * Globals
 *
 */

t_daemon_cfg *cfg;

int brightness = -1;

/*
 * Functions
 *
 */

/*
 * debug()
 * 
 * Note: debug() is derived from the debug routines of OpenSSH (http://www.openssh.org)
 *
 * Gives simple debug messages on stderr if -d flag is set.
 *
 * debug() should only be called, if cfg already resists in the shared memory segment...
 *
 */

void debug(const char *fmt, ...)
{
    if (cfg->debug_flag == 1) {
        va_list args;
        char msgbuf[MSGBUFSIZE];

        va_start(args, fmt);

        fprintf(stdout, "debug: ");
        vsnprintf(msgbuf, sizeof(msgbuf), fmt, args);
        fprintf(stdout, "%s\r\n", msgbuf);

        va_end(args);
    }
}

/*
 * fatal()
 *
 * Error output on stderr.
 *
 */

void fatal(const char *fmt, ...)
{
    va_list args;
    char msgbuf[MSGBUFSIZE];

    va_start(args, fmt);

    fprintf(stderr, "\nfatal error: ");
    vsnprintf(msgbuf, sizeof(msgbuf), fmt, args);
    fprintf(stderr, "%s\r\n\n", msgbuf);

    va_end(args);
}

/*
 * version()
 *
 * Print version on stdout.
 *
 */

void version()
{
    fprintf(stdout, PROGRAM " " VERSION " (c) 2003, 2004 " AUTHOR "\n");
}

/*    
 * information()
 *
 * Show some usefull information when starting.
 * 
 */

void information()
{
    int value = 0;

    fprintf(stdout, "Status: LCD Brightness %3.1f%%",
            (float) (((brightness_status() + 1)) * 12.5));

    if (cfg->acpi.config & VAL_FAN)
        fprintf(stdout, ", Fan is %s", fan_status()? "running" : "not running");

    if (cfg->acpi.config & VAL_VIDEO) {
        value = video_status();
        fprintf(stdout, ", LCD is %s, CRT is %s, TV-Out is %s",
                (value & VIDEO_LCD) ? "enabled" : "disabled",
                (value & VIDEO_CRT) ? "enabled" : "disabled",
                (value & VIDEO_TV) ? "enabled" : "disabled");
    }

    fprintf(stdout, ".\n");
}

/*
 * usage()
 *
 * Print usage on stdout.
 *
 */

void usage()
{
    fprintf(stdout,
            "\n"
            "Options:\n"
            "-d: enable debug output.\n"
            "-h: show this help.\n" "-n: do not send into daemon mode.\n");
}

/*
 * check_uid()
 *
 * Check wether we're running as root. We need to be root, to be able
 * to write to proc fs.
 * 
 */

int check_uid()
{
    if (getuid()) {
        fatal("Not running as root. Please run fnfxd with root priviledges.");
        return 1;
    }
    return 0;
}

/*
 * init()
 *
 * Call initialization routines.
 *
 */

int init(int *debug, int *nodetach)
{
    if (check_uid())
        return 1;

    if (init_sig_handler())
        return 1;

    if (init_fnfxd(debug, nodetach))
        return 1;

    return 0;
}

/*
 * loop()
 *
 * Endless loop which polls for key events (Fn-x, hotkeys).
 * 
 */

void loop()
{
    while (1) {
        while (!chk_fnkey()) {
            usleep(300000);
        }
    }
}

/*
 * main()
 *
 */

int main(int ac, char **av)
{
    int i, opt_err, debug_flag, nodetach_flag;

    version();

    opt_err = debug_flag = nodetach_flag = 0;

    if (ac > 1) {
        while ((i = getopt(ac, av, "dhn")) != -1) {
            switch (i) {
            case 'd':
                debug_flag = 1;
                break;
            case 'h':
                opt_err = 1;
                break;
            case 'n':
                nodetach_flag = 1;
                break;
            default:
                opt_err = 1;
            }
        }
    }

    if (opt_err) {
        usage();
        return 0;
    }

    if (init(&debug_flag, &nodetach_flag))
        return 1;

    if (cfg->debug_flag)
        walk_config();

    if (brightness != -1)
        set_brightness(brightness);

    information();

    if (!cfg->nodetach_flag) {
        daemon(0, 0);
        init_pid();
    }

    loop();

    /* We should _never_ get here */

    return 1;
}
