/*
 * Author: Timo Hoenig <thoenig@nouse.net>
 * Copyright (c) 2003, 2004 Timo Hoenig <thoenig@nouse.net>
 *                          All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

/*
 * Defines
 *
 */

/*
 * Declarations
 *
 */

void walk_config(void);

/*
 * Includes
 *
 */

#include "fnfx.h"

/*
 * External functions 
 *
 */

extern void debug(const char *fmt, ...) __attribute__ ((format(printf, 1, 2)));
extern void fatal(const char *fmt, ...) __attribute__ ((format(printf, 1, 2)));

/*
 * External variables
 *
 */

extern t_client_cfg *cfg;
extern t_daemon_cfg *daemon_cfg;

/*
 * Globals
 *
 */

/*
 * Functions
 *
 */

/*
 * config_walk()
 *
 * Just 'walk' the config struct and print as debug messages
 *
 */

void walk_config()
{
    int i = 0;
    struct shmid_ds shm_info;

    debug("config_walk() - Starting config walk...");
    debug(" ");

    debug("I General Information");

    debug("-> FnFX Client PID is %i (getpid() = %i).", cfg->pid, getpid());
    debug("-> FnFX Daemon PID is %i.", daemon_cfg->pid);
    debug("-> Command line arguments: Debug is %s, Nodetach is %s.",
          (cfg->flags & DEBUG) ? "ON" : "OFF",
          (cfg->flags & NODETACH) ? "ON" : "OFF");
    debug("-> Configuration size is %i bytes.", sizeof(*cfg));
    debug(" ");

    debug("II Shared Memory");

    if (shmctl(cfg->shmem.id, IPC_STAT, &shm_info) == -1) {
        debug
            ("Error while retrieving shared memory information. Aborting walk_config().");
        return;
    }
    else

        debug
            ("-> Shared memory segment is located at 0x%x (size: %i bytes, id: %i, key: 0x%x).",
             (unsigned int) cfg->shmem.addr, shm_info.shm_segsz, cfg->shmem.id,
             cfg->shmem.key);
    debug("-> Client configuration is located at 0x%x.", (unsigned int) cfg);
    debug("-> Daemon configuration is located at 0x%x.",
          (unsigned int) daemon_cfg);
    debug(" ");

    debug("III Internal Commands");
    debug("-> ACPI related commands:");

    if (daemon_cfg->acpi.config & VAL_FAN)
        debug("  'toggle_fan'\t\t- Switch fan on/off.");
    if (daemon_cfg->acpi.config & VAL_LCD) {
        debug("  'brightness up'\t- Increase LCD brightness.");
        debug("  'brightness down'\t- Decrease LCD brightness.");
    }
    if (daemon_cfg->acpi.config & VAL_VIDEO) {
        debug("  'toggle video'\t\t- Switch video mode (LCD, LCD & CRT, CRT)");
        debug("  'toggle tv'\t\t- Switch TV out");
    }
    debug(" ");
    debug("-> Other commands:");
    if (daemon_cfg->mixer.config & MIXER_ENABLED) {
        debug("  'volume up'\t\t- Increase mixer (%s) volume.",
              daemon_cfg->mixer.device);
        debug("  'volume down'\t\t- Decrease mixer (%s) volume.",
              daemon_cfg->mixer.device);
        debug("  'mute'\t\t\t- Mute/Unmute mixer (%s).",
              daemon_cfg->mixer.device);
    }
    debug(" ");

    debug("IV Semaphore");
    debug("-> Semaphore id is %i.", cfg->sem.id);
    debug("-> Semaphore value is %i.", semctl(cfg->sem.id, 0, GETVAL));
    debug(" ");

    debug("V ACTIONS");
    for (i = 0; i < daemon_cfg->keymap.entries; i++) {
        if ((daemon_cfg->keymap.fnkey[i].state & FNKEY_ENABLED) &&
            strlen(daemon_cfg->keymap.fnkey[i].client_command))
            debug("-> %s (value = 0x%x) will execute \"%s\".",
                  daemon_cfg->keymap.fnkey[i].descr,
                  daemon_cfg->keymap.fnkey[i].value,
                  daemon_cfg->keymap.fnkey[i].client_command);

    }

    debug(" ");

}
