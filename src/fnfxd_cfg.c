/*
 * Author: Timo Hoenig <thoenig@nouse.net>
 * Copyright (c) 2003, 2004 Timo Hoenig <thoenig@nouse.net>
 *                          All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

/*
 * Defines
 *
 */

#define KEYMAP		"/etc/fnfx/keymap"
#define CONFIGFILE	"/etc/fnfx/fnfxd.conf"

/*
 * Includes
 *
 */

#include "fnfx.h"

/*
 * Declarations
 *
 */

int init_config(void);
void clean_config(void);
int lookup(int);
int load_actions(FILE *, char *);
int load_defaults(FILE *, char *);
int add_action(int, char *);
int is_action(char *);
int load_keymap(FILE *, char *);
int load_mixer(FILE *, char *);

/*
 * External functions
 *
 */

extern void debug(const char *fmt, ...) __attribute__ ((format(printf, 1, 2)));
extern void fatal(const char *fmt, ...) __attribute__ ((format(printf, 1, 2)));

/*
 * External variables
 *
 */

extern t_daemon_cfg *cfg;
extern int brightness;

/*
 * Functions
 *
 */

/*
 * lookup()
 *
 * Return description for fnkey value.
 *
 */

int lookup(int fnkey_value)
{
    int i;

    for (i = 0; i < cfg->keymap.entries; i++)
        if (cfg->keymap.fnkey[i].value == fnkey_value)
            return (i);
    return -1;
}

/*
 * load_keymap()
 *
 * Load the keymap (all known Fn-x combinations and hotkeys.
 * 
 */

int load_keymap(FILE * f, char *line)
{
    int i = 0;

    cfg->keymap.entries = 0;

    while (!feof(f)) {
        fgets(line, LINE_SIZE, f);

        if (!strncmp(line, "key(", 4)) {
            /* Find next free entry of config.keymap.fnkey[] */
            while (cfg->keymap.fnkey[i].value)
                i++;

            /* Fill config.keymap.fnkey[] */
            sscanf(line, "key(value=0x%3x;description=\"%[^\"]\")",
                   &cfg->keymap.fnkey[i].value, cfg->keymap.fnkey[i].descr);

            /* If we have a somewhat malformed config line, remove entry */
            if (!strlen(cfg->keymap.fnkey[i].descr))
                i -= 1;
            else
                cfg->keymap.entries += 1;
        }

    }

    return 0;
}

/*
 * add_action()
 *
 * Add (internal) action to the configuration (in memory).
 * 
 */

int add_action(int i, char *command)
{
    strcpy(cfg->keymap.fnkey[i].command, command);
    cfg->keymap.fnkey[i].state |= FNKEY_ENABLED;
    return 0;
}

/*
 * is_action()
 * 
 * Check wether the given action is a internal command.
 *
 */

int is_action(char *command)
{
    if (!(strcmp("brightness up", command)))
        return 0;
    else if (!(strcmp("brightness down", command)))
        return 0;
    else if (!(strcmp("toggle fan", command)))
        return 0;
    else if (!(strcmp("toggle video", command)))
        return 0;
    else if (!(strcmp("toggle tv", command)))
        return 0;
    else if (!(strcmp("volume up", command)))
        return 0;
    else if (!(strcmp("volume down", command)))
        return 0;
    else if (!(strcmp("mute", command)))
        return 0;
    else if (!(strcmp("suspend to ram", command)))
        return 0;
    else if (!(strcmp("suspend to disk", command)))
        return 0;
    else if (!(strcmp("toggle cpu", command)))
        return 0;
    else if (!(strcmp("toggle bluetooth", command)))
        return 0;
    return 1;
}

/*
 * load_actions()
 *
 * Parse configuration file for actions.
 *
 */

int load_actions(FILE * f, char *line)
{
    int i = 0;

    char fnkey_descr[LINE_SIZE];
    char command[CMD_SIZE];

    while (!feof(f)) {
        fgets(line, LINE_SIZE, f);

        if ((!strncmp(line, "[", 1)))
            break;

        if (!strncmp(line, "action(", 7)) {
            sscanf(line, "action(key=\"%[^\"]\";command=\"%[^\"]\")",
                   fnkey_descr, command);
            for (i = 0; i < cfg->keymap.entries; i++) {
                if (!
                    (strncmp
                     (cfg->keymap.fnkey[i].descr, fnkey_descr,
                      strlen(fnkey_descr)))) {
                    if (strlen(command)) {
                        if (!(is_action(command)))
                            add_action(i, command);
                        else
                            debug
                                ("Error (non-fatal) while reading config file. "
                                 "\"%s\" is no valid command.", command);
                    }
                    else {
                        strcpy(cfg->keymap.fnkey[i].command, "");
                        cfg->keymap.fnkey[i].state &= ~FNKEY_ENABLED;
                    }
                }
            }
        }
    }

    return 0;
}

/*
 * load_mixer()
 *
 * Parse configuration file for mixer definition.
 *
 */

int load_mixer(FILE * f, char *line)
{
    FILE *mixer_device;
    char mixer[MIXER_SIZE];

    while (!feof(f)) {
        fgets(line, LINE_SIZE, f);

        if ((!strncmp(line, "[", 1)))
            break;

        if (!strncmp(line, "mixer(", 6)) {
            sscanf(line, "mixer(device=\"%[^\"]\")", mixer);
            if (strlen(mixer)) {
                strcpy(cfg->mixer.device, mixer);
                mixer_device = fopen(cfg->mixer.device, "r+");

                if (mixer_device) {
                    cfg->mixer.config |= MIXER_ENABLED;
                    cfg->mixer.config &= ~MIXER_MUTE;

                    fclose(mixer_device);
                }
                else {
                    debug
                        ("error (non-fatal): the mixer device (%s) is not available. volume and mute control will be disabled.",
                         cfg->mixer.device);
                    cfg->mixer.config &= ~MIXER_ENABLED;
                    cfg->mixer.config &= ~MIXER_MUTE;
                }
            }
        }
    }

    return 0;
}

/*
 * load_mixer()
 *
 * Parse configuration file for mixer definition.
 *
 */

int load_defaults(FILE * f, char *line)
{
    char defaults[LINE_SIZE];

    while (!feof(f)) {
        fgets(line, LINE_SIZE, f);

        if ((!strncmp(line, "[", 1)))
            break;

        if (!strncmp(line, "defaults(", 6)) {
            sscanf(line, "defaults(brightness=\"%[^\"]\")", defaults);
            if (strlen(defaults)) {
                sscanf(defaults, "%1d", &brightness);
                if (brightness > 7)
                    brightness = 7;
                if (brightness < 0)
                    brightness = 0;
            }
        }
    }
    return 0;
}

/*
 * init_config()
 *
 * Call the initialization routines.
 * 
 */

int init_config()
{
    FILE *f;
    int i;

    char line[LINE_SIZE];

    if ((f = fopen(KEYMAP, "r"))) {
        while (!feof(f)) {
            fgets(line, sizeof(line), f);

            if (!strncmp(line, "[keys]", 6))
                load_keymap(f, line);
        }

    }
    else {
        fatal
            ("Could not open \"%s\". Please make sure that the keymap is accessible.",
             KEYMAP);
        return 1;
    }

    if (f)
        fclose(f);

    if ((f = fopen(CONFIGFILE, "r"))) {
        while (!feof(f)) {
            fgets(line, sizeof(line), f);

            if (!strncmp(line, "[actions]", 8))
                load_actions(f, line);

            if (!strncmp(line, "[mixer]", 7))
                load_mixer(f, line);

            if (!strncmp(line, "[defaults]", 7))
                load_defaults(f, line);
        }

    }
    else {
        fatal
            ("Could not open \"%s\". Please make sure that the default config is accessible.",
             CONFIGFILE);
        return 1;
    }

    if (f)
        fclose(f);

    /* Check if configuration is valid */
    for (i = 0; i < cfg->keymap.entries; i++) {
        if (cfg->keymap.fnkey[i].state & FNKEY_ENABLED) {
            /* If mixer is _not_ available. We have to check wether volume up (down) is used. */
            if (cfg->mixer.config ^ MIXER_ENABLED) {
                if ((!(strcmp(cfg->keymap.fnkey[i].command, "volume up"))) ||
                    (!(strcmp(cfg->keymap.fnkey[i].command, "volume down"))) ||
                    (!(strcmp(cfg->keymap.fnkey[i].command, "mute")))
                    ) {
                    debug
                        ("error (non-fatal): The command \"%s\" is used for %s, but the mixer device \"%s\" is not available. "
                         "Disabling defined action.",
                         cfg->keymap.fnkey[i].command,
                         cfg->keymap.fnkey[i].descr, cfg->mixer.device);
                    strcpy(cfg->keymap.fnkey[i].command, "");
                    cfg->keymap.fnkey[i].state &= ~FNKEY_ENABLED;

                }
            }
            if (!(cfg->acpi.config & VAL_VIDEO)) {
                if ((!(strcmp(cfg->keymap.fnkey[i].command, "toggle video"))) ||
                    (!(strcmp(cfg->keymap.fnkey[i].command, "toggle tv")))
                    ) {
                    debug
                        ("error (non-fatal): The command \"%s\" is used for %s, but \"%s\" is not available. "
                         "Disabling defined action.",
                         cfg->keymap.fnkey[i].command,
                         cfg->keymap.fnkey[i].descr, ACPI_VIDEO);
                    strcpy(cfg->keymap.fnkey[i].command, "");
                    cfg->keymap.fnkey[i].state &= ~FNKEY_ENABLED;

                }
            }
            if (!(cfg->acpi.config & VAL_SLEEP)) {
                if ((!(strcmp(cfg->keymap.fnkey[i].command, "suspend to ram")))
                    ||
                    (!(strcmp(cfg->keymap.fnkey[i].command, "suspend to disk")))
                    ) {
                    debug
                        ("error (non-fatal): The command \"%s\" is used for %s, but \"%s\" is not available. "
                         "Disabling defined action.",
                         cfg->keymap.fnkey[i].command,
                         cfg->keymap.fnkey[i].descr, ACPI_SLEEP);
                    strcpy(cfg->keymap.fnkey[i].command, "");
                    cfg->keymap.fnkey[i].state &= ~FNKEY_ENABLED;
                }
            }
        }
    }

    return 0;
}

/*
 * clean_config()
 *
 * If a client did _not_ a proper clean up, we take this job.
 *
 */

void clean_config()
{
    int i;

    for (i = 0; i < cfg->keymap.entries; i++) {
        if ((cfg->keymap.fnkey[i].state & FNKEY_ENABLED) &&
            strlen(cfg->keymap.fnkey[i].client_command)) {
            strcpy(cfg->keymap.fnkey[i].client_command, "");

            if (strlen(cfg->keymap.fnkey[i].command))
                cfg->keymap.fnkey[i].state |= FNKEY_ENABLED;
            else
                cfg->keymap.fnkey[i].state &= ~FNKEY_ENABLED;

        }

    }
}
