/* fnfx.c - FnFX client
 * 
 * Author: Timo Hoenig <thoenig@nouse.net>
 * Copyright (c) 2003, 2004 Timo Hoenig <thoenig@nouse.net>
 *                          All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

/*
 * Defines
 *
 */

#define PROGRAM		"FnFX Client"
#define	VERSION		"v0.3"
#define AUTHOR		"Timo Hoenig <thoenig@nouse.net>"

/*
 * Declarations
 *
 */

void debug(const char *, ...);
void fatal(const char *, ...);

void wait_fnfxd(void);
void signal_fnfxd(void);

void version(void);
void usage(void);
void loop(void);

/*
 * Includes
 *
 */

#include "fnfx.h"

/*
 * External functions 
 *
 */

extern int init_fnfx(int *);
extern int init_sig_handler(void);
extern int walk_config(void);
extern int handle_fnkey(void);

/*
 * External variables
 *
 */

/*
 * Globals
 *
 */

t_client_cfg *cfg;
t_daemon_cfg *daemon_cfg;

/*
 * Functions
 *
 */

/*
 * debug()
 * 
 * Note: debug() is derived from the debug routines of OpenSSH (http://www.openssh.org)
 *
 * Gives simple debug messages on stderr if -d flag is set.
 *
 * debug() should only be called, if cfg already resists in the shared memory segment...
 */

void debug(const char *fmt, ...)
{
    if (cfg->flags & DEBUG) {
        va_list args;
        char msgbuf[MSGBUFSIZE];

        va_start(args, fmt);

        fprintf(stdout, "debug: ");
        vsnprintf(msgbuf, sizeof(msgbuf), fmt, args);
        fprintf(stdout, "%s\r\n", msgbuf);

        va_end(args);
    }
}

/*
 * fatal()
 *
 * Error output on stderr.
 *
 */

void fatal(const char *fmt, ...)
{
    va_list args;
    char msgbuf[MSGBUFSIZE];

    va_start(args, fmt);

    fprintf(stderr, "\nfatal error: ");
    vsnprintf(msgbuf, sizeof(msgbuf), fmt, args);
    fprintf(stderr, "%s\r\n\n", msgbuf);

    va_end(args);
}

/*
 * wait_fnfxd()
 *
 * Waiting for a key event (Fn-x, hotkey).
 *
 */

void wait_fnfxd()
{
    cfg->sem.op.sem_num = 1;
    cfg->sem.op.sem_op = -1;
    cfg->sem.op.sem_flg = 0;
    semop(cfg->sem.id, &cfg->sem.op, 1);
}

/*
 * signal_fnfxd()
 *
 * Showing fnfxd that we have reacted.
 *
 */

void signal_fnfxd()
{
    cfg->sem.op.sem_num = 2;
    cfg->sem.op.sem_op = 1;
    cfg->sem.op.sem_flg = 0;
    if (semop(cfg->sem.id, &cfg->sem.op, 1)) {
        fatal("Lost connection to daemon `fnfxd`.");
        exit(1);
    }
}

/*
 * version()
 *
 * Print program version on stdout.
 * 
 */

void version()
{
    fprintf(stdout, PROGRAM " " VERSION " (c) 2003, 2004 " AUTHOR "\n");
}

/*
 * usage()
 *
 * Print usage on stdout.
 * 
 */

void usage()
{
    fprintf(stdout,
            "\n"
            "Options:\n"
            "-d: enable debug output.\n"
            "-h: show this help.\n" "-n: do not send into daemon mode.\n");
}

/*
 * init()
 *
 * Call initialization routines of fnfx on start.
 *
 */

int init(int *flags)
{
    if (init_sig_handler())
        return 1;

    if (init_fnfx(flags))
        return 1;

    return 0;
}

/*
 * loop()
 *
 * Endless loop which reacts on key events (Fn-x, hotkeys).
 *
 */

void loop()
{
    printf("enter");
    while (1) {
        debug("loop() - Waiting for Fn-key combination.");
        wait_fnfxd();
        debug("loop() - Received Fn-key combination (%s).",
              daemon_cfg->acpi.fnkey_descr);
        handle_fnkey();
        signal_fnfxd();
    }

}

/*
 * main()
 *
 */

int main(int ac, char **av)
{
    int i, flags;

    version();

    flags = 0;

    if (ac > 1) {
        while ((i = getopt(ac, av, "dhn")) != -1) {
            switch (i) {
            case 'd':
                flags |= DEBUG;
                break;
            case 'h':
                flags |= OPT_ERROR;
                break;
            case 'n':
                flags |= NODETACH;
                break;
            default:
                flags |= OPT_ERROR;
            }
        }
    }

    if (flags & OPT_ERROR) {
        usage();
        return 0;
    }

    if (init(&flags))
        return 1;

    if (cfg->flags & DEBUG)
        walk_config();

    if (!(cfg->flags & NODETACH)) {
        daemon(0, 1);
        daemon_cfg->client_pid = getpid();
    }

    loop();

    return 0;
}
