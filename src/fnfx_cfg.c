/*
 * Author: Timo Hoenig <thoenig@nouse.net>
 * Copyright (c) 2003, 2004 Timo Hoenig <thoenig@nouse.net>
 *                          All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

/*
 * Defines
 *
 */

#define CONFIGFILE	".fnfxrc"

/*
 * Includes
 *
 */

#include "fnfx.h"

/*
 * Declarations
 *
 */

int handle_fnkey(void);

int add_action(int, char *);
int add_external(int, char *);
int is_action(char *);
int init_config(void);
void clean_config(void);

/*
 * External functions
 *
 */

extern void debug(const char *fmt, ...) __attribute__ ((format(printf, 1, 2)));
extern void fatal(const char *fmt, ...) __attribute__ ((format(printf, 1, 2)));

/*
 * External variables
 *
 */

extern t_client_cfg *cfg;
extern t_daemon_cfg *daemon_cfg;

/*
 * Functions
 *
 */

int handle_fnkey(void)
{
    int i = daemon_cfg->acpi.fnkey_pos;

    if ((daemon_cfg->keymap.fnkey[i].state & FNKEY_ENABLED) &&
        strlen(daemon_cfg->keymap.fnkey[i].client_command)) {
        if (daemon_cfg->keymap.fnkey[i].state & FNKEY_EXTERNAL) {
            char command[CMD_SIZE];
            sprintf(command, "%s%1c",
                    daemon_cfg->keymap.fnkey[i].client_command, '&');

            debug("handle_fnkey() - executing: %s", command);
            system(command);
        }
        else
//          if(strlen(daemon_cfg->keymap.fnkey[i].client_command))
            debug
                ("handle_fnkey() - Nothing to handle. It's a internal command and executed by the daemon.");
    }
    else
        debug
            ("handle_fnkey() - Nothing to do. There is no configuration entry for this key.");

    return 0;
}

/*
 * add_action()
 *
 * Add (internal) action to the configuration (in memory).
 *
 */

int add_action(int i, char *command)
{
    strcpy(daemon_cfg->keymap.fnkey[i].client_command, command);
    daemon_cfg->keymap.fnkey[i].state |= FNKEY_ENABLED;
    return 0;
}

/*
 * add_external()
 *
 * Add (external) action to the configuration (in memory).
 *
 */

int add_external(int i, char *command)
{
    strcpy(daemon_cfg->keymap.fnkey[i].client_command, command);
    daemon_cfg->keymap.fnkey[i].state |= FNKEY_ENABLED;
    daemon_cfg->keymap.fnkey[i].state |= FNKEY_EXTERNAL;
    return 0;
}

/*
 * is_action()
 *
 * Check wether the given action is a internal command.
 *
 */

int is_action(char *command)
{
    if (!(strcmp("brightness up", command)))
        return 0;
    else if (!(strcmp("brightness down", command)))
        return 0;
    else if (!(strcmp("toggle fan", command)))
        return 0;
    else if (!(strcmp("toggle video", command)))
        return 0;
    else if (!(strcmp("toggle tv", command)))
        return 0;
    else if (!(strcmp("volume up", command)))
        return 0;
    else if (!(strcmp("volume down", command)))
        return 0;
    else if (!(strcmp("mute", command)))
        return 0;
    else if (!(strcmp("suspend to ram", command)))
        return 0;
    else if (!(strcmp("suspend to disk", command)))
        return 0;

    return 1;
}

/*
 * load_actions()
 *
 * Parse configuration file for actions.
 *
 */

int load_actions(FILE * f, char *line)
{
    int i = 0;
    char fnkey_descr[LINE_SIZE];
    char command[CMD_SIZE];

    while (!feof(f)) {
        fgets(line, LINE_SIZE, f);

        if ((!strncmp(line, "[", 1)))
            break;

        if (!strncmp(line, "action(", 7)) {
            sscanf(line, "action(key=\"%[^\"]\";command=\"%[^\"]\")",
                   fnkey_descr, command);

            if ((strlen(fnkey_descr)) && (strlen(command))) {
                for (i = 0; i < daemon_cfg->keymap.entries; i++) {
                    if (!
                        (strncmp
                         (daemon_cfg->keymap.fnkey[i].descr, fnkey_descr,
                          strlen(fnkey_descr)))) {
                        if (strlen(command)) {
                            if (!(is_action(command)))
                                add_action(i, command);
                            else
                                add_external(i, command);
                        }
                        else {
                            strcpy(daemon_cfg->keymap.fnkey[i].client_command,
                                   "");
                            //daemon_cfg->keymap.fnkey[i].state = 0;
                        }
                    }

                }
            }
        }
    }

    return 0;
}

/*
 * init_config()
 *
 * Call the initialization routines.
 * 
 */

int init_config()
{
    FILE *f;
    int i = 0;

    char config_file[LINE_SIZE];
    char line[LINE_SIZE];

    sprintf(config_file, "%s%1c%s%1c", getenv("HOME"), '/', CONFIGFILE, '\0');

    if ((f = fopen(config_file, "r"))) {
        while (!feof(f)) {
            fgets(line, sizeof(line), f);

            if (!strncmp(line, "[actions]", 8))
                load_actions(f, line);
        }

    }
    else {
        fatal
            ("Could not open \"%s\". Please make sure that the default config is accessible.",
             config_file);
        return 1;
    }

    if (f)
        fclose(f);

    for (i = 0; i < daemon_cfg->keymap.entries; i++) {
        if (daemon_cfg->keymap.fnkey[i].state & FNKEY_ENABLED) {
            if (daemon_cfg->mixer.config ^ MIXER_ENABLED) {
                if ((!
                     (strcmp
                      (daemon_cfg->keymap.fnkey[i].client_command,
                       "volume up"))) ||
                    (!(strcmp
                       (daemon_cfg->keymap.fnkey[i].client_command,
                        "volume down"))) ||
                    (!(strcmp
                       (daemon_cfg->keymap.fnkey[i].client_command, "mute")))
                    ) {
                    debug
                        ("error (non-fatal): The command \"%s\" is used for %s, but the mixer device \"%s\" is not available. "
                         "Disabling defined action.",
                         daemon_cfg->keymap.fnkey[i].client_command,
                         daemon_cfg->keymap.fnkey[i].descr,
                         daemon_cfg->mixer.device);
                    strcpy(daemon_cfg->keymap.fnkey[i].client_command, "");
                    daemon_cfg->keymap.fnkey[i].state &= 0;
                }
            }
            if (!(daemon_cfg->acpi.config & VAL_VIDEO)) {
                if ((!
                     (strcmp
                      (daemon_cfg->keymap.fnkey[i].client_command,
                       "toggle video"))) ||
                    (!(strcmp
                       (daemon_cfg->keymap.fnkey[i].client_command,
                        "toggle tv")))
                    ) {
                    debug
                        ("error (non-fatal): The command \"%s\" is used for %s, but \"%s\" is not available. "
                         "Disabling defined action.",
                         daemon_cfg->keymap.fnkey[i].client_command,
                         daemon_cfg->keymap.fnkey[i].descr, ACPI_VIDEO);
                    strcpy(daemon_cfg->keymap.fnkey[i].client_command, "");
                    daemon_cfg->keymap.fnkey[i].state = 0;
                }
            }
            if (!(daemon_cfg->acpi.config & VAL_SLEEP)) {
                if ((!
                     (strcmp
                      (daemon_cfg->keymap.fnkey[i].client_command,
                       "suspend to ram"))) ||
                    (!(strcmp
                       (daemon_cfg->keymap.fnkey[i].client_command,
                        "suspend to disk")))
                    ) {
                    debug
                        ("error (non-fatal): The command \"%s\" is used for %s, but \"%s\" is not available. "
                         "Disabling defined action.",
                         daemon_cfg->keymap.fnkey[i].client_command,
                         daemon_cfg->keymap.fnkey[i].descr, ACPI_SLEEP);
                    strcpy(daemon_cfg->keymap.fnkey[i].client_command, "");
                    daemon_cfg->keymap.fnkey[i].state = 0;

                }
            }
        }
    }

    return 0;
}

/*
 * clean_config()
 *
 * Cleanup configuration (in memory) when stopping fnfx.
 * This restores the daemon configuration (when no client is connected).
 *
 */

void clean_config()
{
    int i;

    for (i = 0; i < daemon_cfg->keymap.entries; i++) {
        if ((daemon_cfg->keymap.fnkey[i].state & FNKEY_ENABLED) &&
            strlen(daemon_cfg->keymap.fnkey[i].client_command)) {
            strcpy(daemon_cfg->keymap.fnkey[i].client_command, "");

            if (strlen(daemon_cfg->keymap.fnkey[i].command))
                daemon_cfg->keymap.fnkey[i].state |= FNKEY_ENABLED;
            else
                daemon_cfg->keymap.fnkey[i].state &= ~FNKEY_ENABLED;

        }

    }
}
