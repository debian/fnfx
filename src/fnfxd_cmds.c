/*
 * Author: Timo Hoenig <thoenig@nouse.net>
 * Copyright (c) 2003, 2004 Timo Hoenig <thoenig@nouse.net>
 *                          All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

/*
 * Defines
 *
 */

/*
 * Declarations
 *
 */

int get_volume(int *);
int volume_up(void);
int volume_down(void);
int mute(void);
int mute(void);
int brightness_status(void);
int brightness_up(void);
int brightness_down(void);
int fan_toggle(void);
int fan_status(void);
int video_status(void);
int video_toggle(int);
int suspend(int);
int cpu_toggle(void);
int cpu_status(void);
int bluetooth_toggle(void);
int bluetooth_status(void);

/*
 * Includes
 *
 */

#include <sys/ioctl.h>
#include <linux/soundcard.h>
#include <fcntl.h>

#include "fnfx.h"

/*
 * External functions 
 *
 */

extern void debug(const char *fmt, ...) __attribute__ ((format(printf, 1, 2)));
extern void fatal(const char *fmt, ...) __attribute__ ((format(printf, 1, 2)));

/*
 * External variables
 *
 */

extern t_daemon_cfg *cfg;

/*
 * Globals
 *
 */

/*
 * Functions
 *
 */

int get_volume(int *value)
{
    int mixer = open(cfg->mixer.device, O_RDONLY);

    if (mixer) {
        ioctl(mixer, SOUND_MIXER_READ_VOLUME, value);
        close(mixer);
        return 0;
    }
    else
        return 1;
}

int set_volume(int *value)
{
    int mixer = open(cfg->mixer.device, O_RDWR);

    if (mixer) {
        ioctl(mixer, SOUND_MIXER_WRITE_VOLUME, value);
        close(mixer);
        return 0;
    }
    else
        return 1;
}

int volume_up()
{
    int value = 0;

    get_volume(&value);

    if (value < 0x5a5a)
        value += 0x0a0a;
    else
        value = 0x6464;

    set_volume(&value);

    if (cfg->mixer.config & MIXER_MUTE)
        cfg->mixer.config &= ~MIXER_MUTE;

    return 0;
}

int volume_down()
{
    int value = 0;

    get_volume(&value);

    if (value > 0x0a0a)
        value -= 0x0a0a;
    else
        value = 0;

    set_volume(&value);

    if (cfg->mixer.config & MIXER_MUTE)
        cfg->mixer.config &= ~MIXER_MUTE;

    return 0;
}

int mute()
{
    int value = 0;

    if (cfg->mixer.config & MIXER_MUTE) {
        set_volume(&cfg->mixer.volume);
        cfg->mixer.config &= ~MIXER_MUTE;
    }
    else {
        get_volume(&cfg->mixer.volume);
        set_volume(&value);
        cfg->mixer.config |= MIXER_MUTE;
    }

    return 0;
}

int brightness_status()
{
    FILE *f;
    int value = 0;

    f = fopen(ACPI_LCD, "r");

    if (!f)
        return -1;
    else
        fscanf(f, "brightness:%1d", &value);

    fclose(f);
    return value;
}

int brightness_up()
{
    FILE *f;
    int value = 0;

    f = fopen(ACPI_LCD, "r+");

    if (!f) {
        debug("Error while opening %s. Could not adjust brightness.", ACPI_LCD);
        return 1;
    }
    else {
        fscanf(f, "brightness:%1d", &value);
        fprintf(f, "brightness:%1d", ++value);
    }

    fclose(f);
    return 0;
}

int brightness_down()
{
    FILE *f;
    int value = 0;

    f = fopen(ACPI_LCD, "r+");

    if (!f) {
        debug("Error while opening %s. Could not adjust brightness.", ACPI_LCD);
        return 1;
    }
    else {
        fscanf(f, "brightness:%1d", &value);
        fprintf(f, "brightness:%1d", --value);
    }

    fclose(f);
    return 0;
}

int set_brightness(int brightness)
{
    FILE *f;
    int value = 0;

    f = fopen(ACPI_LCD, "r+");

    if (!f) {
        debug("Error while opening %s. Could not adjust brightness.", ACPI_LCD);
        return 1;
    }
    else {
        fprintf(f, "brightness:%1d", brightness);
    }

    fclose(f);
    return 0;
}

int fan_status()
{
    int value;

    FILE *f = fopen(ACPI_FAN, "r");

    if (!f)
        return -1;
    else
        fscanf(f, "running:%1d", &value);

    fclose(f);

    return value;
}

int fan_toggle()
{
    int value = 0;
    FILE *f = fopen(ACPI_FAN, "r+");

    if (!f) {
        debug("Error while opening %s. Could not change fan settings.",
              ACPI_FAN);
        return 1;
    }
    else {
        fscanf(f, "running:%1d", &value);
        fprintf(f, "force_on:%1d", value ? 0 : 1);
    }

    fclose(f);
    return 0;
}

int video_toggle(int interface)
{
    int value = 0;
    FILE *f = fopen(ACPI_VIDEO, "r+");

    value = video_status();

    if (!f) {
        debug("Error while opening %s. Could not change video settings.",
              ACPI_VIDEO);
        return 1;
    }
    else {
        value = video_status();

        switch (interface) {
        case VIDEO_LCD:
            switch ((value & VIDEO_LCD) + (value & VIDEO_CRT)) {
            case 0x1:
                fprintf(f, "lcd_out:%1d;crt_out:%1d", 1, 1);
                printf("read: lcd=%i, crt=%i\t", (value & VIDEO_LCD) ? 1 : 0,
                       (value & VIDEO_CRT) ? 1 : 0);
                printf("write: lcd=%i, crt=%i.\n", 1, 1);
                break;
            case 0x2:
                fprintf(f, "lcd_out:%1d;crt_out:%1d", 1, 0);
                printf("read: lcd=%i, crt=%i\t", (value & VIDEO_LCD) ? 1 : 0,
                       (value & VIDEO_CRT) ? 1 : 0);
                printf("write: lcd=%i, crt=%i.\n", 1, 0);
                break;
            case 0x3:
                fprintf(f, "lcd_out:%1d;crt_out:%1d", 0, 1);
                printf("read: lcd=%i, crt=%i\t", (value & VIDEO_LCD) ? 1 : 0,
                       (value & VIDEO_CRT) ? 1 : 0);
                printf("write: lcd=%i, crt=%i.\n", 0, 1);
                break;
            default:
                return 1;
            }
            break;
        case VIDEO_TV:
            fprintf(f, "tv_out:%1d", (value & VIDEO_TV) ? 0 : 1);
            break;
        default:
            return 1;
        }
    }

    fclose(f);
    return 0;
}

int video_status(void)
{
    int value[4] = { 0, 0, 0, 0 };
    FILE *f = fopen(ACPI_VIDEO, "r");

    if (!f) {
        debug("Error while opening %s. Could not read video status.",
              ACPI_VIDEO);
        return 0;
    }
    else {
        fscanf(f, "lcd_out:%1d\ncrt_out:%1d\ntv_out:%1d",
               &value[0], &value[1], &value[2]
            );

        if (value[0])
            value[3] |= VIDEO_LCD;
        if (value[1])
            value[3] |= VIDEO_CRT;
        if (value[2])
            value[3] |= VIDEO_TV;
    }
    fclose(f);
    return value[3];
}

int suspend(int state)
{
    FILE *f = fopen(ACPI_SLEEP, "r+");

    if (!f) {
        debug("Error while opening %s. Could not suspend to ram.", ACPI_SLEEP);
        return 0;
    }
    else {
        if (state == 3)
            fprintf(f, "%d", state);
        else if (state == 4)
            fprintf(f, "%d", state);
        else {
            debug("suspend() - Not supported sleep state.");
            return 1;
        }
        fclose(f);
        return 0;
    }
}

int cpu_toggle()
{
    int value = 0;
    FILE *f = fopen(ACPI_CPU, "r+");

    if (!f) {
        debug("Error while opening %s. Could not change cpu settings.",
              ACPI_CPU);
        return 1;
    }
    else {
        fscanf(f, "active limit: P%1d", &value);
        fprintf(f, "%1d:0", value ? 0 : 1);
    }

    fclose(f);
    return 0;
}

int cpu_status()
{
    int value;

    FILE *f = fopen(ACPI_CPU, "r");

    if (!f)
        return -1;
    else
        fscanf(f, "active limit: P%1d", &value);

    fclose(f);

    return value;
}

int bluetooth_toggle()
{
    int value = 0;
    char buffer[64];

    FILE *f = fopen(ACPI_BLUETOOTH, "r+");

    if (!f) {
        debug("Error while opening %s. Could not toggle bluetooth.",
              ACPI_BLUETOOTH);
        return 1;
    }
    else {
        /* why does that not work? */
        /* scanf(f, "blue_on: %1d", &value); */

        while (fgets(buffer, sizeof(buffer) - 1, f)) {
            buffer[sizeof(buffer) - 1] = '\0';
            sscanf(buffer, "blue_on: %d\n", &value);
        }
        fprintf(f, "blue_on:%1d", value ? 0 : 1);
    }

    fclose(f);
    return 0;
}

int bluetooth_status()
{
    int value;
    char buffer[64];

    FILE *f = fopen(ACPI_BLUETOOTH, "r");

    if (!f)
        return -1;
    else {
        while (fgets(buffer, sizeof(buffer) - 1, f)) {
            buffer[sizeof(buffer) - 1] = '\0';
            sscanf(buffer, "blue_on: %d\n", &value);
        }
    }
    fclose(f);

    return value;
}
