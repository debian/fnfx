/*
 * Author: Timo Hoenig <thoenig@nouse.net>
 * Copyright (c) 2003, 2004 Timo Hoenig <thoenig@nouse.net>
 *                          All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include <sys/types.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <math.h>
#include <string.h>

#include <signal.h>

#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>

/* 
 * Defines
 * 
 */

#define DEBUG		0x1
#define	NODETACH	0x2
#define OPT_ERROR	0x4

#define ACPI_TOSHIBA	"/proc/acpi/toshiba"
#define ACPI_FAN	"/proc/acpi/toshiba/fan"
#define ACPI_KEYS	"/proc/acpi/toshiba/keys"
#define ACPI_LCD	"/proc/acpi/toshiba/lcd"
#define ACPI_VERSION	"/proc/acpi/toshiba/version"
#define ACPI_VIDEO	"/proc/acpi/toshiba/video"
#define ACPI_SLEEP	"/proc/acpi/sleep"
#define ACPI_CPU	"/proc/acpi/processor/CPU0/limit"
#define ACPI_BLUETOOTH	"/proc/acpi/toshiba/bluetooth"

#define VAL_TOSHIBA	0x01
#define VAL_FAN		0x02
#define VAL_KEYS	0x04
#define VAL_LCD		0x08
#define VAL_VERSION	0x10
#define VAL_VIDEO	0x20
#define VAL_SLEEP	0x40

#define VIDEO_LCD	0x1
#define VIDEO_CRT	0x2
#define VIDEO_TV	0x4

#define FNKEY_ENABLED	0x1
#define FNKEY_CLIENT	0x2
#define FNKEY_EXTERNAL	0x4

#define MIXER_ENABLED	0x1
#define MIXER_MUTE	0x2

#define SHMKEY		0x31011980
#define SHMSIZE		655360

#define SEMKEY		0x31011980

#define MSGBUFSIZE	1024
#define LINE_SIZE	128
#define CMD_SIZE	128
#define MIXER_SIZE	128

#define	DCFG_OFFSET	1024
#define CCFG_OFFSET	0

#define PIDFILE		"/var/run/fnfxd.pid"

typedef struct {
    int volume;
    int config;
    char device[MIXER_SIZE];
} s_mixer;

typedef struct {
    int value;
    int state;

    char command[CMD_SIZE];
    char client_command[CMD_SIZE];
    char descr[LINE_SIZE];
} s_fnkey;

typedef struct {
    int entries;
    s_fnkey fnkey[128];
} s_keymap;

typedef struct {
    int id;
    key_t key;
    struct sembuf op;
} s_sem_cfg;

typedef struct {
    int id;
    key_t key;
    void *addr;
} s_shmem_cfg;

typedef struct {
    int hotkey_ready;
    int hotkey;

    char *fnkey_descr;
    int fnkey_pos;

    int config;
    FILE *proc_fd;
} s_acpi_cfg;

/* FnFX Client Config */
typedef struct s_client_config {
    int flags;

    pid_t pid;

    s_shmem_cfg shmem;
    s_sem_cfg sem;
} t_client_cfg;

/* FnFX Daemon Config */
typedef struct s_daemon_config {
    int debug_flag;
    int nodetach_flag;

    pid_t pid;
    pid_t client_pid;

    s_shmem_cfg shmem;
    s_sem_cfg sem;
    s_acpi_cfg acpi;
    s_keymap keymap;
    s_mixer mixer;
} t_daemon_cfg;
