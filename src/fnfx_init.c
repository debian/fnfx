/* 
 * Author: Timo Hoenig <thoenig@nouse.net>
 * Copyright (c) 2003, 2004 Timo Hoenig <thoenig@nouse.net>
 *                          All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

/*
 * Defines
 *
 */

/*
 * Includes
 *
 */

#include <sys/ioctl.h>
#include <fcntl.h>

#include "fnfx.h"

/*
 * Declarations
 *
 */

void sigterm_handler(int signal);
int init_sig_handler(void);
int init_fnfx(int *);
int init_shmem(t_client_cfg *);
int cp_cfg(t_client_cfg *);
int set_flags(int *);
int init_pid(void);
int init_daemon_cfg(void);
int check_instance(void);
int init_sem(void);

/*
 * External functions 
 *
 */

extern void debug(const char *, ...);
extern void fatal(const char *, ...);
extern int init_config(void);
extern int clean_config(void);

extern t_client_cfg *cfg;
extern t_daemon_cfg *daemon_cfg;

/*
 * Globals
 *
 */

/*
 * Functions
 *
 */

/*
 * sigterm_handler()
 * 
 * Signal handler for terminating signals.
 *
 * - Remove pid file
 * - Free shared memory
 */

void sigterm_handler(int signal)
{
    debug("Received signal %d. Terminating.", signal);
    /* Clean up daemon configuration */
    clean_config();
    /* Set semaphore 0 and 2 to zero. */
    semctl(cfg->sem.id, 0, SETVAL, 0);
    semctl(cfg->sem.id, 2, SETVAL, 1);
    /* detach shared memory */
    shmdt(cfg->shmem.addr);
    exit(255);
}

/*
 * init_sig_handler()
 *
 * Register the signal handler for terminating signals.
 * 
 */

int init_sig_handler()
{
    signal(SIGTERM, sigterm_handler);
    signal(SIGKILL, sigterm_handler);
    signal(SIGQUIT, sigterm_handler);
    signal(SIGINT, sigterm_handler);

    return 0;
}

/*
 * init_shmem()
 *
 * - Attach shared memory segment
 *
 */

int init_shmem(t_client_cfg * config)
{
    key_t shmkey = (int) SHMKEY;

    config->shmem.key = shmkey;
    config->shmem.id = shmget(config->shmem.key, (int) SHMSIZE, 0666);

    if (config->shmem.id < 0) {
        fatal
            ("Failed to get id of shared memory segment. Please make sure that the daemon 'fnfxd' is running.");
        return 1;
    }

    if ((config->shmem.addr = shmat(config->shmem.id, NULL, 0)) == (char *) -1) {
        fatal("Error while attaching shared memory segment.");
        return 1;
    }

    return 0;
}

/*
 * cp_cfg()
 *
 * - Copy configuration to shared memory segment
 * - Let the global t_client_cfg *cfg point to it
 * 
 */

int cp_cfg(t_client_cfg * config)
{
    memcpy((config->shmem.addr + CCFG_OFFSET), (void *) config,
           sizeof(*config));

    cfg = config->shmem.addr + CCFG_OFFSET;

    return 0;
}

/*
 * set_flags()
 *
 * Set the command line flags in the configuration (in memory).
 * 
 */

int set_flags(int *flags)
{
    cfg->flags = *flags;

    return 0;
}

/*
 * init_pid()
 *
 * Write PID to configuration (in memory).
 *
 */

int init_pid()
{
    cfg->pid = getpid();
    return 0;
}

/*
 * init_daemon_cfg()
 *
 * Set pointer to daemon configuration.
 *
 */

int init_daemon_cfg()
{
    daemon_cfg = cfg->shmem.addr + DCFG_OFFSET;
    return 0;
}

/*
 * check_instance()
 *
 * Check wether another client is running.
 *
 */

int check_instance()
{
    if (daemon_cfg->client_pid) {
        if (!(kill(daemon_cfg->client_pid, 0))) {
            fatal
                ("Another instance of fnfx is running (PID = %i). Please be sure that only one instance of `fnfx` is running.",
                 daemon_cfg->client_pid);
            return 1;
        }
    }

    daemon_cfg->client_pid = getpid();

    return 0;
}

/*
 * init_sem()
 *
 * Get semaphore context.
 *
 */

int init_sem()
{
    cfg->sem.key = SEMKEY;

    cfg->sem.id = semget(cfg->sem.key, 1, 0666);

    if (cfg->sem.id < 0) {
        fatal("Could not get id of semaphore with key 0x%i.", cfg->sem.key);
        return 1;
    }

    if (semctl(cfg->sem.id, 0, GETVAL)) {
        /* We only get here if for example a 'kill -9' on fnfx already had initialized the semaphore */
        semop(cfg->sem.id, &cfg->sem.op, 1);

        return 0;
    }

    cfg->sem.op.sem_num = 0;
    cfg->sem.op.sem_op = 1;
    cfg->sem.op.sem_flg = 0;
    semop(cfg->sem.id, &cfg->sem.op, 1);

    /* If fnfxd is waiting for a client which got 'kill -9' to handle a read fn combination */

    semctl(cfg->sem.id, 2, SETVAL, 1);

    return 0;
}

/*
 * init_fnfx()
 *
 * Call initialization routines.
 *
 */

int init_fnfx(int *flags)
{
    t_client_cfg config;

    if (init_shmem(&config))
        return 1;

    if (cp_cfg(&config))
        return 1;

    if (set_flags(flags))
        return 1;

    if (init_pid())
        return 1;

    if (init_daemon_cfg())
        return 1;

    if (check_instance())
        return 1;

    if (init_config())
        return 1;

    if (init_sem())
        return 1;

    return 0;

}
