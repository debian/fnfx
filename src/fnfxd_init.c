/*
 * Author: Timo Hoenig <thoenig@nouse.net>
 * Copyright (c) 2003, 2004 Timo Hoenig <thoenig@nouse.net>
 *                          All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

/*
 * Defines
 *
 */

/*
 * Includes
 *
 */

#include <sys/ioctl.h>
#include <fcntl.h>

#include "fnfx.h"

/*
 * Declarations
 *
 */

void sigterm_handler(int signal);
int check_pid(void);
int init_sig_handler(void);
int init_fnfxd(int *, int *);
int init_shmem(t_daemon_cfg *);
int cp_cfg(t_daemon_cfg *);
int set_flags(int *, int *);
int init_pid(void);
int init_acpi(void);
int init_sem(void);

/*
 * External functions 
 *
 */

extern void debug(const char *, ...);
extern void fatal(const char *, ...);
extern int init_config(void);

extern t_daemon_cfg *cfg;

/*
 * Globals
 *
 */

/*
 * Functions
 *
 */

/*
 * sigterm_handler()
 * 
 * Signal handler for terminating signals.
 *
 * - Remove pid file
 * - Free shared memory
 */

void sigterm_handler(int signal)
{
    debug("Received signal %d. Terminating.", signal);
    /* destory shared memory */
    shmctl(cfg->shmem.id, IPC_RMID, cfg->shmem.addr);
    /* remove semaphore */
    semctl(cfg->sem.id, 0, IPC_RMID);
    /* remove pid file */
    unlink(PIDFILE);
    exit(255);
}

/*
 * init_sig_handler()
 *
 * Register the signal handler for terminating signals.a
 *
 */

int init_sig_handler()
{
    signal(SIGTERM, sigterm_handler);
    signal(SIGKILL, sigterm_handler);
    signal(SIGQUIT, sigterm_handler);
    signal(SIGINT, sigterm_handler);

    return 0;
}

/*
 * check_pid()
 * 
 * Check if another instance of fnfxd is running.
 * 
 */

int check_pid()
{
    FILE *f;
    pid_t pid, read_pid;

    pid = (unsigned int) getpid();
    read_pid = 0;

    f = fopen(PIDFILE, "r+");

    if (f) {
        fscanf(f, "%i", &read_pid);
        fatal
            ("According to %s, another instance of `fnfxd` is already running (PID = %i).\n"
             "If this is not the case, please remove %s.", PIDFILE, read_pid,
             PIDFILE);
        fclose(f);
        return 1;
    }

    return 0;
}

/*
 * init_shmem()
 *
 * - Initialize shared memory segment
 * - Attach shared memory segment
 *
 */

int init_shmem(t_daemon_cfg * config)
{
    key_t shmkey = (int) SHMKEY;

    config->shmem.key = shmkey;
    config->shmem.id =
        shmget(config->shmem.key, (int) SHMSIZE, IPC_CREAT | 0666);

    if (config->shmem.id < 0) {
        fatal("Failed to create shared memory segment.");
        return 1;
    }

    if ((config->shmem.addr = shmat(config->shmem.id, NULL, 0)) == (char *) -1) {
        fatal("Error while attaching shared memory segment.");
        return 1;
    }

    return 0;
}

/*
 * cp_cfg()
 *
 * - Copy configuration to shared memory segment
 * - Let the global t_daemon_cfg *cfg point to it
 *   
 */

int cp_cfg(t_daemon_cfg * config)
{
    memcpy((config->shmem.addr + DCFG_OFFSET), (void *) config,
           sizeof(*config));

    cfg = config->shmem.addr + DCFG_OFFSET;

    return 0;
}

/*
 * set_flags()
 *
 * Set the command line flags in the configuration (in memory).
 * 
 */

int set_flags(int *debug_flag, int *nodetach_flag)
{
    cfg->debug_flag = *debug_flag;
    cfg->nodetach_flag = *nodetach_flag;

    return 0;
}

/*
 * init_pid()
 *
 * Check for other instances of fnfxd and register PID file.
 *
 */

int init_pid()
{
    FILE *f;

    cfg->pid = getpid();
    f = fopen(PIDFILE, "w");

    if (f) {
        fprintf(f, "%u\n", cfg->pid);
        fclose(f);
    }
    else {
        fatal("Could not access pid file %s.", PIDFILE);
        return 1;
    }

    return 0;
}

/*
 * init_acpi()
 *
 * - Check if /proc/acpi/toshiba and subkeys are available
 *
 */

int init_acpi()
{
    FILE *f;
    int i;

    const char *acpi_proc_keys[9] = {
        ACPI_TOSHIBA,
        ACPI_FAN,
        ACPI_KEYS,
        ACPI_LCD,
        ACPI_VERSION,
        ACPI_VIDEO,
        ACPI_SLEEP,
        ACPI_CPU,
        ACPI_BLUETOOTH
    };

    for (i = 1; i < 7; i++) {
        if ((f = fopen(acpi_proc_keys[i], "r+"))) {
            cfg->acpi.config |= 1 << i;
            fclose(f);
        }
        else if ((i == 2) || (i == 3)) {

            fatal("Could open %s.", acpi_proc_keys[i]);
            fprintf(stderr,
                    "Please make sure that your kernel has enabled the Toshiba option in the ACPI section.\n"
                    "For more information read the documentation and/or http://fnfx.sf.net/index.php?section=doc#kernel.\n\n");
            return 1;
        }
    }

    cfg->acpi.config |= 1 << 0;

    return 0;
}

/*
 * init_sem()
 *
 * Initialize semaphores.
 *
 */

int init_sem()
{
    cfg->sem.key = SEMKEY;

    cfg->sem.id = semget(cfg->sem.key, 3, IPC_CREAT | 0666);
    if (cfg->sem.id < 0) {
        fatal("Could not create semaphore with key 0x%i.", cfg->sem.key);
        return 1;
    }

    if (semctl(cfg->sem.id, 0, SETVAL, 0)) {
        fatal("Could not initialize semaphore %i (id = %i) with 0.", 0,
              cfg->sem.id);
        return 1;
    }
    if (semctl(cfg->sem.id, 1, SETVAL, 0)) {
        fatal("Could not initialize semaphore %i (id = %i) with 0.", 1,
              cfg->sem.id);
        return 1;
    }
    if (semctl(cfg->sem.id, 2, SETVAL, 0)) {
        fatal("Could not initialize semaphore %i (id = %i) with 0.", 2,
              cfg->sem.id);
        return 1;
    }

    return 0;
}

/*
 * init_fnfxd()
 *
 * Call initialization routines.
 *
 */

int init_fnfxd(int *debug_flag, int *nodetach_flag)
{
    t_daemon_cfg config;

    if (check_pid())
        return 1;

    if (init_shmem(&config))
        return 1;

    if (cp_cfg(&config))
        return 1;

    if (set_flags(debug_flag, nodetach_flag))
        return 1;

    if (init_acpi())
        return 1;

    if (init_config())
        return 1;

    if (init_sem())
        return 1;

    return 0;
}
