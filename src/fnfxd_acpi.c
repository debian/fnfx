/*
 * Author: Timo Hoenig <thoenig@nouse.net>
 * Copyright (c) 2003, 2004 Timo Hoenig <thoenig@nouse.net>
 *                          All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

/*
 * Defines
 *
 */

/*
 * Declarations
 *
 */

int chk_fnkey(void);
void signal_fnfx(void);
void wait_fnfx(void);
int notify_client(void);

/*
 * Includes
 *
 */

#include "fnfx.h"

/*
 * External functions
 *
 */

extern void debug(const char *fmt, ...) __attribute__ ((format(printf, 1, 2)));
extern void fatal(const char *fmt, ...) __attribute__ ((format(printf, 1, 2)));
extern int lookup(int);
extern int do_action(int);
extern void clean_config(void);

/*
 * External variables
 *
 */

extern t_daemon_cfg *cfg;

/*
 * Functions
 *
 */

/*
 * signal_fnfx()
 *
 * Show fnfx that we have a new key event (Fn-x, hotkey).
 *
 */

void signal_fnfx()
{
    cfg->sem.op.sem_num = 1;
    cfg->sem.op.sem_op = 1;
    cfg->sem.op.sem_flg = 0;
    semop(cfg->sem.id, &cfg->sem.op, 1);
}

/*
 * wait_fnfx()
 *
 * Wait for fnfx, showing that it reacted on the key event (Fn-x, hotkey).
 *
 */

void wait_fnfx()
{
    cfg->sem.op.sem_num = 2;
    cfg->sem.op.sem_op = -1;
    cfg->sem.op.sem_flg = 0;
    if (!(semop(cfg->sem.id, &cfg->sem.op, 1)))
        debug("wait_fnfx() - Client has read key.");
    else
        debug("wait_fnfx() - Client timeout.");
}

/*
 * notify_client()
 *
 * return 1 if client is _NOT_ connected
 * => chk_fnkey executes command (cfg->keymap.fnkey[].command if defined
 *
 * return 0 if client is connected
 * => chk_fnkey executes command (cfg->keymap.fnkey[].client_command if defined
 *
 */

int notify_client()
{
    if (semctl(cfg->sem.id, 0, GETVAL)) {
        debug("notify_client() - Client is connected.");
        if (kill(cfg->client_pid, 0)) {
            /* The client has died and could not clean up */
            cfg->client_pid = 0;
            semctl(cfg->sem.id, 0, SETVAL, 0);
            clean_config();
        }
        else {
            /* We have a key to 'consume'. Is the client ready? */
            signal_fnfx();

            /* He is. Waiting for clients 'consumption'. */
            wait_fnfx();
        }
        return 0;
    }
    else
        return 1;
}

/*
 * chk_fnkey()
 *
 * Scan /proc/acpi/toshiba/keys for Fn-key events.
 *
 * If a new key event is found return 1, otherwise 0.
 *
 */

int chk_fnkey()
{
    int fnkey_pos = -1;

    cfg->acpi.proc_fd = fopen(ACPI_KEYS, "r+");

    if (!cfg->acpi.proc_fd)
        debug("chk_fnkey() - Could not open %s.", ACPI_KEYS);

    else {
        fscanf(cfg->acpi.proc_fd, "hotkey_ready: %d\nhotkey: 0x%4x",
               &cfg->acpi.hotkey_ready, &cfg->acpi.hotkey);

        if (cfg->acpi.hotkey_ready) {
            fnkey_pos = lookup(cfg->acpi.hotkey);
            cfg->acpi.fnkey_descr = cfg->keymap.fnkey[fnkey_pos].descr;
            cfg->acpi.fnkey_pos = fnkey_pos;

            if (fnkey_pos != -1) {
                if (notify_client()) {
                    if (cfg->debug_flag)
                        debug("chk_fnkey() - %s (0x%x). Executing command: %s.",
                              cfg->keymap.fnkey[fnkey_pos].descr,
                              cfg->acpi.hotkey,
                              ((cfg->keymap.fnkey[fnkey_pos].
                                state) & FNKEY_ENABLED) ? cfg->keymap.
                              fnkey[fnkey_pos].command : "None");

                    if (cfg->keymap.fnkey[fnkey_pos].state & FNKEY_ENABLED) {
                        cfg->keymap.fnkey[fnkey_pos].state &= ~FNKEY_CLIENT;
                        do_action(fnkey_pos);
                    }
                }
                else {
                    if (strlen(cfg->keymap.fnkey[fnkey_pos].client_command)) {
                        if (!
                            (cfg->keymap.fnkey[fnkey_pos].
                             state & FNKEY_EXTERNAL)) {
                            if (cfg->debug_flag)
                                debug
                                    ("chk_fnkey() - %s (0x%x). Executing command for client: %s.",
                                     cfg->keymap.fnkey[fnkey_pos].descr,
                                     cfg->acpi.hotkey,
                                     ((cfg->keymap.fnkey[fnkey_pos].
                                       state) & FNKEY_ENABLED) ? cfg->keymap.
                                     fnkey[fnkey_pos].client_command : "None");

                            cfg->keymap.fnkey[fnkey_pos].state |= FNKEY_CLIENT;
                            do_action(fnkey_pos);
                        }
                    }
                }

            }
            fprintf(cfg->acpi.proc_fd, "hotkey_ready:0");
            fclose(cfg->acpi.proc_fd);
            return 1;
        }
        fclose(cfg->acpi.proc_fd);
    }

    return 0;
}
